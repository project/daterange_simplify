<?php

namespace Drupal\daterange_simplify\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;

/**
 * Wrapper for flack/ranger daterange simplifier.
 *
 * @FieldFormatter(
 *   id = "intl_formatter",
 *   label = @Translation("Intl"),
 *   field_types = {"date", "datetime"}
 * )
 */
class IntlFormatter extends SimplifyFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'time_format' => 'short',
      'date_format' => 'medium',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $allowed_date_options = $this->simplify::getAllowedFormats();
    $allowed_time_options = $this->simplify::getAllowedFormats(TRUE);

    $form['date_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#options' => array_combine($allowed_date_options, $allowed_date_options),
      '#default_value' => $this->getSetting('date_format'),
    ];

    $form['time_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Time format'),
      '#options' => array_combine($allowed_time_options, $allowed_time_options),
      '#default_value' => $this->getSetting('time_format'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [];

    $summary[] = $this->simplify->datetime(new DrupalDateTime(),
                    $settings['date_format'],
                    $settings['time_format'],
                    $this->languageManager->getCurrentLanguage()->getId()
    );

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();
    $date_only = ($this->getFieldSetting('datetime_type') === DateTimeItem::DATETIME_TYPE_DATE);
    $tz_override = $this->getSetting('timezone_override');

    foreach ($items as $delta => $item) {
      $time = $this->simplify->toDrupalDateTime($item->value, $tz_override, $date_only);

      $simplified = $this->simplify->datetime($time,
        $settings['date_format'],
        $settings['time_format'],
        $langcode
      );

      $elements[$delta] = [
        '#markup' => $simplified,
        '#cache' => [
          'contexts' => [
            'timezone',
          ],
        ],
      ];
    }

    return $elements;
  }

}
