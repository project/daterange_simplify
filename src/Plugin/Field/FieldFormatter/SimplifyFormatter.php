<?php

namespace Drupal\daterange_simplify\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItem;

/**
 * Wrapper for flack/ranger daterange simplifier.
 *
 * @FieldFormatter(
 *   id = "daterange_simplify",
 *   label = @Translation("Simplify"),
 *   field_types = {"daterange"}
 * )
 */
class SimplifyFormatter extends SimplifyFormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'time_format' => 'short',
      'date_format' => 'medium',
      'range_separator' => '-',
      'date_time_separator' => ', ',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $allowed_date_options = $this->simplify::getAllowedFormats();
    $allowed_time_options = $this->simplify::getAllowedFormats(TRUE);

    $form['date_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Date format'),
      '#options' => array_combine($allowed_date_options, $allowed_date_options),
      '#default_value' => $this->getSetting('date_format'),
    ];

    $form['time_format'] = [
      '#type' => 'select',
      '#title' => $this->t('Time format'),
      '#options' => array_combine($allowed_time_options, $allowed_time_options),
      '#default_value' => $this->getSetting('time_format'),
    ];

    $form['range_separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Range separator'),
      '#default_value' => $this->getSetting('range_separator'),
    ];

    $form['date_time_separator'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Date-time separator'),
      '#default_value' => $this->getSetting('date_time_separator'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $settings = $this->getSettings();
    $summary = [];

    $summary[] = $this->t('2 hours apart: @sample', [
      '@sample' => $this->simplify->daterange(new DrupalDateTime(), (new DrupalDateTime())->add(new \DateInterval('PT2H')),
                    $settings['date_format'],
                    $settings['time_format'],
                    $settings['range_separator'],
                    $settings['date_time_separator'],
                    $this->languageManager->getCurrentLanguage()->getId()
      ),
    ]);

    $summary[] = $this->t('2 days apart: @sample', [
      '@sample' => $this->simplify->daterange(new DrupalDateTime(), (new DrupalDateTime())->add(new \DateInterval('P2D')),
                    $settings['date_format'],
                    $settings['time_format'],
                    $settings['range_separator'],
                    $settings['date_time_separator'],
                    $this->languageManager->getCurrentLanguage()->getId()
      ),
    ]);

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $settings = $this->getSettings();
    $date_only = ($this->getFieldSetting('datetime_type') === DateTimeItem::DATETIME_TYPE_DATE);
    $tz_override = $this->getSetting('timezone_override');

    foreach ($items as $delta => $item) {
      $start = $this->simplify->toDrupalDateTime($item->value, $tz_override, $date_only);
      // If there is no end_value, set the end time to the start time.
      $end = $item->end_value ? $this->simplify->toDrupalDateTime($item->end_value, $tz_override, $date_only) : $start;

      $simplified = $this->simplify->daterange($start, $end,
        $settings['date_format'],
        $settings['time_format'],
        $settings['range_separator'],
        $settings['date_time_separator'],
        $langcode
      );

      $elements[$delta] = [
        '#markup' => $simplified,
        '#cache' => [
          'contexts' => [
            'timezone',
          ],
        ],
      ];
    }

    return $elements;
  }

}
