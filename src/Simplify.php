<?php
namespace Drupal\daterange_simplify;

use OpenPsa\Ranger\Ranger;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;

/**
 * Daterange simplification tasks.
 */
class Simplify {

  /**
   * Return allowed formats.
   *
   * See http://php.net/manual/en/class.intldateformatter.php.
   *
   * @param bool $restrict_intl
   *   Limit options available in the absence of intl support.
   *
   * @return array
   *   Possible options.
   */
  public static function getAllowedFormats($restrict_intl = FALSE) {
    if ($restrict_intl) {
      return ['none', 'short'];
    }
    return ['none', 'full', 'long', 'medium', 'short'];
  }

  /**
   * Helper function: return an enumerated constant for the format.
   */
  protected static function getDateFormat($format) {
    switch ($format) {
      case 'none':
        return \IntlDateFormatter::NONE;

      case 'full':
        return \IntlDateFormatter::FULL;

      case 'long':
        return \IntlDateFormatter::LONG;

      case 'medium':
        return \IntlDateFormatter::MEDIUM;

      case 'short':
        return \IntlDateFormatter::SHORT;
    }
    return \IntlDateFormatter::MEDIUM;
  }

  /**
   * Simplify a date range.
   */
  public static function daterange(DrupalDateTime $start, DrupalDateTime $end, $date_format = 'medium', $time_format = 'short', $range_separator = null, $date_time_separator = null, $locale = 'en') {
    $date_format = Simplify::getDateFormat($date_format);
    $time_format = Simplify::getDateFormat($time_format);

    $ranger = new Ranger($locale);
    $ranger
      ->setDateType($date_format)
      ->setTimeType($time_format);
    if (!is_null($date_time_separator)) {
      $ranger->setDateTimeSeparator($date_time_separator);
    }
    if (!is_null($range_separator)) {
      $ranger->setRangeSeparator($range_separator);
    }
    $start = $start->format('c');
    if (empty($end)) {
      $end = $start;
    }
    else {
      $end = $end->format('c');
    }
    return $ranger->format($start, $end);
  }

  /**
   * Simplify a single date/time.
   */
  public static function datetime(DrupalDateTime $time, $date_format = 'medium', $time_format = 'short', $locale = 'en') {
    $date_format = Simplify::getDateFormat($date_format);
    $time_format = Simplify::getDateFormat($time_format);

    $ranger = new Ranger($locale);
    $ranger
      ->setDateType($date_format)
      ->setTimeType($time_format);

    $time = $time->format('c');
    return $ranger->format($time, $time);
  }

  /**
   * Correct for user timezone, convert to DrupalateTime.
   */
  public static function toDrupalDateTime($datetime, $tz_override = NULL, bool $date_only = FALSE) {
    // There are a few ways dates are stored in the datetime field.
    //   1. Date-only (2019-12-09)
    //   2. Date + time and All Day: ISO 8601 (2004-02-12T15:19:21+00:00)
    // We'll also check if this is a unixtime.
    if (is_numeric($datetime)) {
      $format = 'U';
    }
    elseif (strlen($datetime) == 10) {
      $format = 'Y-m-d';
    }
    else {
      $format = 'Y-m-d\TH:i:s';
    }

    // Times are stored in the DB as UTC.
    $ddt = DrupalDateTime::createFromFormat($format, $datetime, 'UTC');

    // Set timezone based on user.
    self::setTimeZone($ddt, $date_only);

    // Override timezone per field setting?
    if ($timezone = $tz_override) {
      $ddt->setTimeZone(timezone_open($timezone));
    }

    return $ddt;
  }

  /**
   * From Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeFormatterBase.
   *
   * Sets the proper time zone on a DrupalDateTime object for the current user.
   *
   * A DrupalDateTime object loaded from the database will have the UTC time
   * zone applied to it.  This method will apply the time zone for the current
   * user, based on system and user settings.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime $date
   *   A DrupalDateTime object.
   * @param bool $date_only
   *   Field is date only.
   */
  protected static function setTimeZone(DrupalDateTime $date, bool $date_only = FALSE) {
    if ($date_only) {
      // A date without time has no timezone conversion.
      $timezone = DateTimeItemInterface::STORAGE_TIMEZONE;
    }
    else {
      $timezone = date_default_timezone_get();
    }
    $date->setTimeZone(timezone_open($timezone));
  }

}
